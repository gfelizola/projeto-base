'use strict';

module.exports = function(grunt) {
    require("matchdep").filterDev("grunt-*").forEach(grunt.loadNpmTasks);

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

        dirs: {
            base : ".",
            js   : "./js",
            sass : "./sass",
            css  : "./css",
            img  : "./img"
        },

        // Server de DEV.
        connect: {
            server: {
                options: {
                    port: 9000,
                    livereload: true,
                    open: true
                }
            }
        },

        // Concat
        concat: {
            options: {
                separator: ';'
            },
            frameworks: {
                src: [
                    // jQuery
                    'bower_components/jquery/jquery.js'
                ],
                dest: 'js/.temp/frameworks.js'
            },
            vendor: {
                src: [
                    // "<%= dirs.js %>/vendor/path.min.js"
                ],
                dest: "<%= dirs.js %>/.temp/vendor.js"
            },
            app: {
                src: ["<%= dirs.js %>/app.*.js"],
                dest: "<%= dirs.js %>/.temp/base.js"
            },
            dev: {
                src: [
                    "<%= dirs.js %>/.temp/frameworks.js",
                    "<%= dirs.js %>/.temp/vendor.js",
                    "<%= dirs.js %>/.temp/base.js"
                ],
                dest: "<%= dirs.js %>/script.js"
            }
        },
        // Uglify
        uglify: {
            options: {
                banner: '/*! <%= pkg.name %> - v<%= pkg.version %> - <%= grunt.template.today("yyyy-mm-dd") %> */',
                properties: true,
                compress: {
                    global_defs: {
                        "DEBUG": false
                    },
                    dead_code: true
                }
            },
            target: {
                files: {
                    'js/script.js': ['js/script.js']
                }
            }
        },
        // Compass
        compass: {
            dev: {
                options: {
                    config: '<%= dirs.base %>/config.dev.rb'
                }
            },
            prod: {
                options: {
                    config: '<%= dirs.base %>/config.rb'
                }
            }
        },

        // Watch
        watch: {
           options: {
                livereload: true
            },
            sass: {
                files: "<%= dirs.sass %>/**/*.scss",
                tasks: ["compass"],
                options: {
                    livereload: false
                }
            },
            css: {
                files: ["<%= dirs.base %>/*.css"]
            },
            js: {
                files: ["Gruntfile.js","<%= dirs.js %>/*.js"],
                tasks: ["concat"]
            },
            others: {
                files: ["<%= dirs.base %>/*.{html,txt}","<%= dirs.img %>/*.{gif,jpg,png}"]
            }
        },

        copy: {
            deploy: {
                files: [
                    // css
                    {src: ['css/*'], dest: '.deploy/', filter:'isFile'},
                    // js
                    {src: ['js/script.js'], dest: '.deploy/js/script.js'},
                    {src: ['js/modernizr.min.js'], dest: '.deploy/js/modernizr.min.js'},
                    {src: ['js/html5.respond.min.js'], dest: '.deploy/js/html5.respond.min.js'},
                    // img
                    {src: ['img/*'], dest: '.deploy/', filter: 'isFile'},
                    // outros
                    {src: ['*.html'], dest: '.deploy/', filter: 'isFile'}
                    // ,{src: ['*.xml'], dest: '.deploy/', filter: 'isFile'},
                    // {src: ['*.txt'], dest: '.deploy/', filter: 'isFile'}
                ]
            }
        },

        rsync: {
            options: {
                args: ["-ravzup"],
                exclude: [".git*","*.scss",".sass-cache","node_modules","bower_components"],
                syncDestIgnoreExcl: true
            },
            fenix: {
                options: {
                    src: "./.deploy/",
                    dest: "/Volumes/Web/ProjetoTribo/PastaProjeto"
                }
            },
            stage: {
                options: {
                    src: "./.deploy/",
                    dest: "/var/www/site",
                    host: "user@staging-host"
                }
            },
            prod: {
                options: {
                    src: "./.deploy/",
                    dest: "/var/www/site",
                    host: "user@live-host"
                }
            }
        }
    });
    grunt.registerTask('b', ['concat', 'uglify', 'compass:prod']);
    grunt.registerTask("w", ["b","watch"]);
    grunt.registerTask("dp", ["b","copy"]);
    grunt.registerTask("fenix", ["b","copy","rsync:fenix"]);
    
    grunt.registerTask("default", ["b"]);
};
